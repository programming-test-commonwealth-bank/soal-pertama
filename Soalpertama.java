import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Soalpertama {
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);

        int n = 0;
        System.out.print("n = ");
        n = Integer.parseInt(br.readLine());
        if (n < 0) {
            System.out.println("Tidak bisa angka di bawah 0");
        } else if (n > 100) {
            System.out.println("Tidak bisa angka di atas 100");
        } else {
            bintang(n);
        }

    }

    public static void bintang(int n) {
        for (int i = 0; i < n; i++) {
            for (int j = n - 1; j > i; j--) {
                System.out.print("  ");
            }
            for (int k = 0; k <= i; k++) {
                System.out.print("# ");
            }
            System.out.println("");
        }
    }
}